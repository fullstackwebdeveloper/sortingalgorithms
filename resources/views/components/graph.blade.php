@php
    $barCount = count($results);
    $barWidth = 500 / $barCount;
    $maxDuration = 0;
    $minDuration = PHP_FLOAT_MAX;
    $minColor = [0,255,0];
    $maxColor = [255,0,0];


    for ($i=0; $i < count($results); $i++)
    { 
        $time = $results[$i]['time'];
        if($time > $maxDuration)
        {
            $maxDuration = $time;
        }
        if($time < $minDuration)
        {
            $minDuration = $time;
        }
    }

@endphp

<div class="flex justify-center m-5">
    <svg class="max-w-[95vw] m:max-w-[85vw] lg:max-w-[75vw] xl:max-w-[50vw]" viewBox="0 0 500 380" xmlns="http://www.w3.org/2000/svg">
        <rect fill="#222222" width="500" height="210" x="0" y="0" rx="0" ry="0"/>
        @for ($i = 0; $i < $barCount; $i++)
            @php
                $time = $results[$i]['time'];
                $height = ($time / $maxDuration) * 200;
                $offset = 210 - $height;
                $fontSize = 12;
                $textXPos = ($i + .5) * $barWidth - $fontSize * .5;

                $colorPosition = ($time - $minDuration) / ($maxDuration - $minDuration);

                $colorR = $maxColor[0] * $colorPosition + $minColor[0] * (1 - $colorPosition);
                $colorG = $maxColor[1] * $colorPosition + $minColor[1] * (1 - $colorPosition);
                $colorB = $maxColor[2] * $colorPosition + $minColor[2] * (1 - $colorPosition);

                $hexColor = sprintf("#%02x%02x%02x", $colorR, $colorG, $colorB);
                

            @endphp
            <g>
                <rect class="hover:fill-yellow-500 peer" fill="{{$hexColor}}" width="{{$barWidth * .9}}" height="{{$height}}" x="{{($i + .05) * $barWidth}}" y="{{$offset}}" rx="0" ry="0" />
                <text class="pointer-events-none fill-white peer-hover:fill-black stroke-[.3px] stroke-black peer-hover:stroke-white" text-anchor="end" font-weight="bold" x="{{$textXPos}}" y="200" font-size="{{$fontSize}}" transform="rotate(90 {{$textXPos}},200)">{{$time / 1000}}ms</text>
                <text class="fill-black peer-hover:fill-orange-500" text-anchor="start" font-weight="bold" x="{{$textXPos}}" y="213" font-size="{{$fontSize}}" transform="rotate(90 {{$textXPos}},213)">{{$results[$i]['name']}}</text>
            </g>
            @endfor
    
    
    </svg>
</div>