<?php

namespace App;

enum QuickSortPivot
{
    case Last;
    case First;
    case Middle;
    case Random;

}

class Sorting
{
    public static function bubbleSort(array $a)
    {
        $copy = $a;
        $startTime = hrtime(true);
        $moved = true;
        while ($moved) {
            $moved = false;
            for ($i = 0; $i < count($copy) - 1; $i++)
            {
                if($copy[$i] > $copy[$i + 1])
                {
                    $temp = $copy[$i];
                    $copy[$i] = $copy[$i + 1];
                    $copy[$i + 1] = $temp;
                    $moved = true;
                }
            }
        }
        // return [$copy, hrtime(true) - $startTime];
        return ["name" => "Bubble Sort", "result" => $copy, "time" => hrtime(true) - $startTime];
    }


    public static function quickSort(array $a, QuickSortPivot $pivotChoice)
    {
        $copy = $a;
        $startTime = hrtime(true);
        self::quick($copy, 0, count($copy) - 1, $pivotChoice);
        // return [$copy, hrtime(true) - $startTime];

        $name = match($pivotChoice)
        {
            QuickSortPivot::First => "First",
            QuickSortPivot::Last => "Last",
            QuickSortPivot::Middle => "Middle",
            QuickSortPivot::Random => "Random"
        };
        return ["name" => "Quick Sort (Pivot: " . $name . ")", "result" => $copy, "time" => hrtime(true) - $startTime];

    }

    public static function insertionSort(array $a)
    {
        $copy = $a;
        $startTime = hrtime(true);
        for ($i = 1; $i < count($copy); $i++)
        {
            for ($j = 0; $j < $i; $j++)
            {
                if($copy[$j] > $copy[$i])
                {
                    array_splice( $copy, $j, 0, [$copy[$i]]);
                    array_splice( $copy, $i + 1, 1);
                    break;
                }
            }
        }
        // return [$copy, hrtime(true) - $startTime];
        return ["name" => "Insertion Sort", "result" => $copy, "time" => hrtime(true) - $startTime];
    }

    public static function shellSort(array $a)
    {
        $copy = $a;
        $startTime = hrtime(true);

        $gap[0] = intval(count($copy) / 2);
        
        $k = 0;
        while($gap[$k] > 1)
        {
            $k++;
            $gap[$k] = intval($gap[$k - 1] / 2);
        }
        
        for ($i = 0; $i <= $k; $i++)
        {
            $step = $gap[$i];
            for ($j = $step; $j < count($copy); $j++)
            {
                $temp = $copy[$j];
                $p = $j - $step;
                while ($p >= 0 && $temp < $copy[$p])
                {
                    $copy[$p + $step] = $copy[$p];
                    $p = $p - $step;
                }
                $copy[$p + $step] = $temp;
            }
        }
        // return [$copy, hrtime(true) - $startTime];
        return ["name" => "Shell Sort", "result" => $copy, "time" => hrtime(true) - $startTime];
    }

    public static function selectionSort(array $a)
    {
        $copy = $a;
        $startTime = hrtime(true);

        for ($i = count($copy) - 1; $i > 0; $i--)
        {
            $max = PHP_FLOAT_MIN;
            $index = -1;
            for ($j = 0; $j <= $i; $j++)
            {
                if($copy[$j] > $max)
                {
                    $max = $copy[$j];
                    $index = $j;
                }
            }
            $temp = $copy[$i];
            $copy[$i] = $copy[$index];
            $copy[$index] = $temp;
        }

        // return [$copy, hrtime(true) - $startTime];
        return ["name" => "Selection Sort", "result" => $copy, "time" => hrtime(true) - $startTime];
    }

    private static function quick(array &$a, int $min, int $max, QuickSortPivot $pivotChoice)
    {
        if($max - $min <= 0)
        {
            return;
        }

        $pivotPos = self::partition($a, $min, $max, $pivotChoice);
        self::quick($a, $min, $pivotPos - 1, $pivotChoice);
        self::quick($a, $pivotPos + 1, $max, $pivotChoice);
    }

    private static function partition(array &$a, int $min, int $max, QuickSortPivot $pivotChoice)
    {
        $pivotIndex = match($pivotChoice)
        {
            QuickSortPivot::First => $min,
            QuickSortPivot::Last => $max,
            QuickSortPivot::Middle => intval(($max + $min) / 2),
            QuickSortPivot::Random => rand($min, $max)
        };

        $pivot = $a[$pivotIndex];
        $a[$pivotIndex] = $a[$max];
        $a[$max] = $pivot;
        $i = $min - 1;

        for ($j = $min; $j < $max; $j++)
        {
            if($a[$j] <= $pivot)
            {
                $i++;
                $temp = $a[$i];
                $a[$i] = $a[$j];
                $a[$j] = $temp;
            }
        }
        $i++;
        $temp = $a[$i];
        $a[$i] = $a[$max];
        $a[$max] = $temp;

        return $i;
    }
}