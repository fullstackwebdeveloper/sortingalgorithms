<x-layouts.layout>
    <div class="flex flex-col items-center justify-center min-h-[90vh]">
        <form class="flex flex-col items-center justify-center" action="{{route('process')}}" method="post">
            @csrf
            <textarea class="border-2 border-black rounded-xl p-2" id="list" name="list" rows="10" cols="50" placeholder="Please enter the list of values to sort, separated by commas."></textarea>
            @if (session('error'))
                <p class="text-[#ff0000] font-bold">
                    {{session('error')}}
                </p>
            @endif
            <button class="bg-lime-500 rounded-full px-5 py-2 font-bold mt-3" type="submit" value="submit">SORT !</button>
        </form>

        <h2 class="font-bold text-xl mt-5" >Some examples :</h2>
        <x-premadelist id="example1" serie="1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20" />
        <x-premadelist id="example2" serie="20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1" />
        <x-premadelist id="example3" serie="1,3,5,7,9,11,13,15,17,19,20,18,16,14,12,10,8,6,4,2" />
        <x-premadelist id="example4" serie="20,18,16,14,12,10,8,6,4,2,1,3,5,7,9,11,13,15,17,19" />
    </div>
</x-layouts.layout>