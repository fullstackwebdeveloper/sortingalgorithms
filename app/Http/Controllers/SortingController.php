<?php

namespace App\Http\Controllers;


use App\QuickSortPivot;
use App\Sorting;
use Illuminate\Http\Request;


class SortingController extends Controller
{
    public function show(){
        return view("form");
    }

    public function processForm(Request $request){
        
        if(!isset($request->list)){
            return redirect()->back()->with("error","Please enter something to sort");
        }

        $input = explode(",", $request->list);

        $results =
        [
            Sorting::bubbleSort($input),
            Sorting::insertionSort($input),
            Sorting::shellSort($input),
            Sorting::selectionSort($input),
            Sorting::quickSort($input, QuickSortPivot::Last),
            Sorting::quickSort($input, QuickSortPivot::First),
            Sorting::quickSort($input, QuickSortPivot::Middle),
            Sorting::quickSort($input, QuickSortPivot::Random),
        ];

        return view("results", ["results" => $results]);
    }
}
