<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sorting Algorithms</title>
    @vite('resources/css/app.css')
</head>
<body class="bg-gray-200">
    <div class="flex items-center justify-center font-bold text-3xl bg-black text-white w-full h-[5vh]">
        Sorting Algorithms
    </div>
    {{$slot}}
    <footer class="static bottom-0 h-[5vh] flex items-center justify-center bg-black text-white">
        &copy; 2024 Alexis Zampiero - All rights reserved
    </footer>
</body>
</html>