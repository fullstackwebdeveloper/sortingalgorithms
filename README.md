<img src="/sortIcon.png" alt="SortIcon logo" style="height: 100px; width:100px;"/>

## Sorting Algorithms

This project allows you to compare different sorting algorithms and see their performance on a graph, according to the time spent by each algorithm to sort the same series of data, in ascending order.

The currently implemented algorithms are:

- Bubble Sort
- Insertion Sort
- Shell Sort
- Selection Sort
- Quick Sort
    - Pivot on last
    - Pivot on first
    - Pivot at middle
    - Pivot random



## How to install

1. Clone the project
2. Go to the project directory and open a terminal
3. Run the following commands:
```shell
composer install
npm install
php artisan serve
```
4. In a separate terminal window, run:
```shell
npm run dev
```

### License

Thi project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
