<x-layouts.layout>
    <x-graph :results="$results" />
    <div class="flex justify-center">
        <a class="bg-lime-500 rounded-full px-5 py-2 font-bold mb-5" href="{{route("show")}}">Retry</a>
    </div>
    <div class="flex flex-col items-center gap-5 mt-5 mb-5">

        @foreach ($results as $result)
            <x-sort-result :result="$result" />
        @endforeach
    </div>
    </x-layouts.layout>