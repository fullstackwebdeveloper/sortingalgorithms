<div class="flex justify-center items-center mt-1">
    <p>{{$serie}}</p>
    <button id="{{$id}}" class="bg-gray-400 rounded-full px-3 py-1 font-bold ml-2" >Try it</button>
</div>
<script>
    document.getElementById("{{$id}}").addEventListener("click", function(){
        document.getElementById("list").value = "{{$serie}}";
    });
</script>