<?php

use App\Http\Controllers\SortingController;
use Illuminate\Support\Facades\Route;

Route::get('/', [SortingController::class,'show'])->name('show');
Route::post('/', [SortingController::class,'processForm'])->name('process');
