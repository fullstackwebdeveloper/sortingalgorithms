<div class="w-[95vw] m:w-[85vw] lg:w-[75vw] xl:w-[50vw] flex flex-col items-center justify-center border-2 border-black rounded-xl text-center">
    <h2 class="font-bold text-3xl">{{$result['name']}}</h2>
    <p class="break-all">Result: {{join(",", $result['result'])}}</p>
    <p>Time: <strong>{{$result['time'] / 1000}}ms</strong></p>
</div>